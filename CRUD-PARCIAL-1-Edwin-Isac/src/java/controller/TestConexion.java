
package controller;

import util.Conexion;


public class TestConexion {
    
    public static void main(String[] args) {
        try {
            Conexion c  = new Conexion();
            c.conectar();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
}
